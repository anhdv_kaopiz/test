<?php

header('Content-type:text/plant');

function debug($d){
	echo '<pre>';
	print_r($d);
	echo '</p>';
	die();
}

function jsonOutput($d,$f){
	header("Content-type:application/json");
	echo json_encode($d);
	if($f)
		die();
}

function find($v,$a = array()){
	foreach ($a as $val) {
		if($val == $v)
			return $v;
	}
	return false;
}
